Kafka for Nameko
================

Make somehting run
------------------

```
docker run --name kafka --rm -p 2181:2181 -p 9092:9092 --env ADVERTISED_HOST=localhost --env ADVERTISED_PORT=9092 spotify/kafka
```

```
pytest
```

Roadmap
-------

* Make a working Entrypoint for use as a `SERVICE_POOL` event handler.

* Implement as an alternative transport for `rpc` and `event_handler` APIs of Nameko.

